<!DOCTYPE html>
<html>
  <head>
    <title>Admin Login</title>
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="assets/styles.css" rel="stylesheet" media="screen">
     <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  </head>
  <body id="login">
    <div class="container">
    
     
      <!-- app/views/login.blade.php -->


  {{ Form::open(array('url' => 'login','class'=>'form-signin')); }}
    <h1>Login</h1>

    <!-- if there are login errors, show them here -->
    <p>
      {{ $errors->first('email') }}
      {{ $errors->first('password') }}
    </p>

    <p>
      {{ Form::label('email', 'Email Address') }}
      {{ Form::text('email', Input::old('email'), ['class' => 'input-block-level']) }}
    </p>

    <p>
      {{ Form::label('password', 'Password') }}
      {{ Form::password('password',null,['class' => 'input-block-level']) }}
    </p>

    <p>{{ Form::submit('Submit!',array('class' => 'btn btn-success')) }}</p>
  {{ Form::close() }}








    </div> <!-- /container -->
    <script src="vendors/jquery-1.9.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>