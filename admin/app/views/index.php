<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Admin Home Page</title>
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
       
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
         <link href="vendors/jGrowl/jquery.jgrowl.css" rel="stylesheet" media="screen">
     <style type="text/css">
        div.jGrowl div.jGrowl-notification.success {
                font-family:            "Helvetica Neue", "Helvetica";
                font-size:              12px;
                background-color:       #468847 !important;
                
            }
            div.jGrowl div.jGrowl-notification.fail {
                font-family:            "Helvetica Neue", "Helvetica";
                font-size:              12px;
                background-color:       #b94a48 !important;
               
            }


            </style>
    </head>
    
    <body>

        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Admin Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i>Grey Cells Admin<i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="#">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="<?php  echo URL::to('/logout')?>">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                       
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="announcement">
                            <a href="#" class="anouncement-section"><i class="icon-chevron-right"></i> Announcement</a>
                        </li>
                        <li class="notice">
                            <a href="#" class="notice-board"><i class="icon-chevron-right"></i>Notice Board</a>
                        </li>
                        <li class="photogallery">
                            <a href="#" class="photo-gallery"><i class="icon-chevron-right"></i>Photo Gallery</a>
                        </li>
                        <li class="videogallery">
                            <a href="#" class="video-gallery"><i class="icon-chevron-right"></i>Video Gallery</a>
                        </li>
                      
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Insert Record</div>
                                    <div class="muted pull-right">
                                     </div>
                                </div>
                                <div class="block-content collapse in">
                               <div class="photogallery-upload">
                                <form class="pic-upload">
                                 <div class="form-pic">
                                   </div>
                                </form>

                                </div>
                                <progress style="display:none"></progress>
                              
                                <form id="multiform" enctype="multipart/form-data">
                                    <div class="controls form-ui">
                                        
                                    </div>
                                </form>
                                </div>

                            </div>    
                    </div>             
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Title</div>
                                    <div class="muted pull-right"></div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Content</th>
                                                <th>Status</th>
                                                <th>Mark</th>
                                                <th>Last Modified</th>
                                                <th>Option</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody class="tableData ">
                                            <img src="img/preview.gif" class="wait" style="display:none" />
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                       
                    </div>
                    
                  
               
            </div>
            <hr>
           
            <footer>
              
            </footer>
        </div>
        	
		<div id="myModal" class="modal hide myModal">
		    <img src="img/preview.gif" class="wait" style="display:none"  />
		</div>
        <div id="myAlert" class="modal hide myModal">
            <img src="img/preview.gif" class="wait" style="display:none"  />
            
            <form id="myalertform" >
            <div class="displayupdatecommand">
            </div>
                

            </form  >
        </div>
        
		
        <!--/.fluid-container-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/scripts.js"></script>
        <script src="vendors/jGrowl/jquery.jgrowl.js"></script>
        <script src="js/custom.js"></script>
     
    </body>

</html>