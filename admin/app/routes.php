<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*Route::get('/',function(){
	return View::make('login');
});*/

Route::get('/', 'HomeController@showWelcome');
Route::get('login', array('uses' => 'HomeController@showWelcome'));
Route::post('login', array('uses' => 'HomeController@doLogin'));
Route::any('logout', array('uses' => 'HomeController@doLogout'));

Route::group(array('before' => 'auth'), function()
{

Route::any('dashboard','HomeController@dashboard');




Route::get('/announcementsection','HomeController@announcementsection');

Route::get('/updateannouncementsection','HomeController@updateannouncementsection');
Route::get('/noticeBoard','HomeController@noticeBoard');
Route::get('/updatenoticeBoard','HomeController@updatenoticeBoard');

Route::get('/contentofannouncement','HomeController@contentofannouncement');

Route::get('/contentofnotice','HomeController@contentofnotice');

Route::post('activedeactiverecords','HomeController@activedeactiverecords');

Route::post('activedeactiverecordsfornotice','HomeController@activedeactiverecordsfornotice');

Route::get('/noticeboard','HomeController@noticeBoard');

Route::get('/photodetails','HomeController@photodetails');
Route::get('/contentofpicture','HomeController@contentofpicture');
Route::post('/activedeactiverecordsforphoto','HomeController@activedeactiverecordsforphoto');

Route::get('/videodetails','HomeController@videodetails');
Route::get('/contentofvideo','HomeController@contentofvideo');
Route::post('/activedeactiverecordsforvideo','HomeController@activedeactiverecordsforvideo');

Route::post('/postdata','HomeController@postdata');
Route::post('/updatedata','HomeController@updatedata');
Route::post('/uploadfile','HomeController@uploadfile');

});
