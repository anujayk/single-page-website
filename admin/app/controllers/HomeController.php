<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('login');
	}
	public function doLogin()
	{
		// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {

			// create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);

			// attempt to do the login
			if (Auth::attempt($userdata)) {

				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				return Redirect::to('dashboard');
				

			} else {	 	

				// validation not successful, send back to form	
				return Redirect::to('dashboard');

			}

		}
	}
	public function doLogout()
	{
		Auth::logout(); // log the user out of our application
		return Redirect::to('/'); // redirect the user to the login screen
	}

	public function dashboard()
	{
		return View::make('index');
	}

	public function announcementsection()
	{
		$result=DB::table('tbl_announcement')->where('status','!=',2)->orderBy('id','DESC')->get();
		$response=json_encode($result);
		return $response;
	}
	public function updateannouncementsection()
	{
		$data=Input::all();
		$result=DB::table('tbl_announcement')->where('status','!=',2)->where('id',$data['id'])->orderBy('id','DESC')->get();
	    $response=json_encode($result);
		return $response;
	}
	public function contentofannouncement()
	{
		$data=Input::all();
		$result=DB::table('tbl_announcement')->where('id',$data['id'])->orderBy('id','DESC')->get();
		$response=json_encode($result);
		return ($response);
	}
	public function activedeactiverecords()
	{
		$data=Input::all();
		if($data['status']=='activ'){
			$status=1;
		}
		else if($data['status']=='deactiv'){
			$status=0;
		}
		$dataToUpdate=array(
			'status' =>$status
			);
		$result=DB::table('tbl_announcement')->where('id',$data['id'])->update($dataToUpdate);
		if($result){
		$response='[{"success":"'.strtoupper($data['status']).'ATED"}]';
		}
		elseif($result){	
		$response='[{"fail":"Some Problem Occured Please Try After Some Times or Contact Support."}]';
		
		}
		return ($response);
	}

	public function noticeBoard()
	{
		$result=DB::table('tbl_notice_board')->where('status','!=',2)->orderBy('id','DESC')->get();

		$response=json_encode($result);
		return ($response);
	}
	public function updatenoticeBoard()
	{
		$data=Input::all();
		$result=DB::table('tbl_notice_board')->where('status','!=',2)->where('id',$data['id'])->orderBy('id','DESC')->get();

		$response=json_encode($result);
		return ($response);
	}

	public function contentofnotice()
	{
		$data=Input::all();
		$result=DB::table('tbl_notice_board')->where('id',$data['id'])->orderBy('id','DESC')->get();
		$response=json_encode($result);
		return ($response);		
	}

	
	public function activedeactiverecordsfornotice()
	{
		$data=Input::all();
		if($data['status']=='activ'){
			$status=1;
		}
		else if($data['status']=='deactiv'){
			$status=0;
		}
		$dataToUpdate=array(
			'status' =>$status
			);
		
		$result=DB::table('tbl_notice_board')->where('id',$data['id'])->update($dataToUpdate);
		if($result){
		$response='[{"success":"'.strtoupper($data['status']).'ATED"}]';
		}
		elseif(!$result){	
		$response='[{"fail":"Some Problem Occured Please Try After Some Times or Contact Support."}]';
		
		}
		return ($response);
	}

	public function photodetails()
	{
		$result=DB::table('tbl_photo_gallery')->where('status','!=',2)->orderBy('id','DESC')->get();
		$response=json_encode($result);
		return ($response);
	}
	public function contentofpicture()
	{
		$data=Input::all();
		$result=DB::table('tbl_photo_gallery')->where('id',$data['id'])->orderBy('id','DESC')->get();
		$response=json_encode($result);
		return ($response);	
	}


	public function activedeactiverecordsforphoto()
	{
		$data=Input::all();
		if($data['status']=='activ'){
			$status=1;
		}
		else if($data['status']=='deactiv'){
			$status=0;
		}
		$dataToUpdate=array(
			'status' =>$status
			);
		//$result=$this->updateData('tbl_photo_gallery',$dataToUpdate,array('id'=>$data['id']));
		$result=DB::table('tbl_photo_gallery')->where('id',$data['id'])->update($dataToUpdate);
		if($result){
		$response='[{"success":"'.strtoupper($data['status']).'ATED"}]';
		}
		elseif(!$result){	
		$response='[{"fail":"Some Problem Occured Please Try After Some Times or Contact Support."}]';
		
		}
		return ($response);
	}




public function videodetails()
	{
		$result=DB::table('tbl_vdo_gallery')->where('status','!=',2)->orderBy('id','DESC')->get();
		$response=json_encode($result);
		return ($response);
	}
	public function contentofvideo()
	{
		$data=Input::all();
		$result=DB::table('tbl_vdo_gallery')->where('id',$data['id'])->orderBy('id','DESC')->get();
		$response=json_encode($result);
		return ($response);	
	}


	public function activedeactiverecordsforvideo()
	{
		$data=Input::all();
		if($data['status']=='activ'){
			$status=1;
		}
		else if($data['status']=='deactiv'){
			$status=0;
		}
		$dataToUpdate=array(
			'status' =>$status
			);
		//$result=$this->updateData('tbl_vdo_gallery',$dataToUpdate,array('id'=>$data['id']));
		$result=DB::table('tbl_vdo_gallery')->where('id',$data['id'])->update($dataToUpdate);
	
		if($result){
		$response='[{"success":"'.strtoupper($data['status']).'ATED"}]';
		}
		elseif(!$result){	
		$response='[{"fail":"Some Problem Occured Please Try After Some Times or Contact Support."}]';
		
		}
		return ($response);
	}
	public function insertData($table_name,$arrayToInsert){

	}
	public function updateDataDetails($table_name,$arrayToUpdate,$arrayClause){

	$result=DB::table($table_name)->where($arrayClause)->update($arrayToUpdate);
	if($result){
		return "success";
	}
	elseif(!$result) {
		return "fail";
	}			
	}

public function postdata()
	{
		$data=Input::all();

		

		if(isset($data['announcementtitle']) && isset($data['announcementcontent'])){
			$tablename="tbl_announcement";
			$dataInsert=array(
				'title'   => $data['announcementtitle'],
				'content' => $data['announcementcontent']
				);
		}
		elseif(isset($data['noticetitle']) && isset($data['noticetitle'])){
			$tablename="tbl_notice_board";
			$dataInsert=array(
				'title'   => $data['noticetitle'],
				'content' => $data['noticecontent'],
				'mark'    =>$data['noticemarknew']  
				);
		}
		elseif(isset($data['phototitle']) && isset($data['photofile'])) {
			$tablename="tbl_photo_gallery";
			$dataInsert=array(
				'title'   => $data['phototitle'],
				'content' => $data['photocontent']				
				);
		}
		elseif(isset($data['videotitle']) && isset($data['videocontent'])){
			$tablename="tbl_vdo_gallery";
			$dataInsert=array(
				'title'   => $data['videotitle'],
				'content' => $data['videocontent']				
				);
		}


		$dataInsertdb=DB::table($tablename)->insert($dataInsert);
/*		$queries = DB::getQueryLog();
$last_query = end($queries);
echo json_encode($last_query);*/
		if($dataInsert){
			echo "success";
		}
		else{
			echo "fail";
		}
	}


	public function updatedata()
	{
		$data=Input::all();

		if(isset($data['updatenoticetitle']) && isset($data['updatenoticecontent'])){

			$tablename="tbl_notice_board";
			$dataToUpdate=array(
					
				'title'      =>$data['updatenoticetitle'],
				'content'    =>$data['updatenoticecontent'] 
				);
		}
		elseif (isset($data["updateanouncementtitle"]) && isset($data["updateanouncementcontent"])) {
			$tablename="tbl_announcement";
			$dataToUpdate=array(
				
				 'title'      =>$data['updateanouncementtitle'],
				 'content'    =>$data['updateanouncementcontent'] 
				);
		}

		$response=DB::table($tablename)->where('id',$data['id'])->update($dataToUpdate);
		if($response){

			echo "success";
		}
		else{
			echo "fail";
		}
	}


	public function uploadfile()
	{
		$data=Input::all();
		//$data2-=Input::file('file');
		$destinationPath = 'photogallery';
		$filename=$data['file'];
		$upload_success = Input::file('file')->move($destinationPath, $filename->getClientOriginalName());
		$tablename="tbl_photo_gallery";
		$dataInsert=array(
			'content'    =>$filename->getClientOriginalName()
			);
		$dataInsertdb=DB::table($tablename)->insert($dataInsert);

		if( $upload_success && $dataInsert) {
		   echo 'success';
		} else {
		   echo 'error';
		}

	}
}
