$(function(){
	$('.wait').show();
	var status;
	var tabledata;
	var toggle;
	$('.form-ui').show();
	$("body").on('click','.anouncement-section',function(){
	$('.photogallery').removeClass("active");
	$('.videogallery').removeClass("active");
	$('.announcement').removeClass("active");
	$('.notice').removeClass("active");
	$('.photogallery-upload').hide();
	
		 tabledata='';
	$.ajax({
		url:'announcementsection',
		type:'GET',
		success:function(response){
			
			var jsonData=JSON.parse(response);
			console.log(response);
			var sno=1;
			$.each(jsonData,function(key,val){
				if(val.status==1){
					status="<span class='label label-success'>Active</span>";
					toggle='<button class="btn btn-default sm toggle" data="deactiv" value="'+val.id+'">Deactivate</button>';
				}
				else if(val.status==0){
					status="<span class='label label-important'>Deactive</span>";					
					toggle='<button class="btn btn-default sm toggle" data="activ" value="'+val.id+'">Activate</button>';
				}


			
				 tabledata+='<tr>'+
									'<td>'+sno+'</td>'+
									'<td>'+val.title+'</td>'+
									'<td><a href="#myModal" data-toggle="modal" class="btn btn-primary content" value="'+val.id+'">View</a>'+
									'</td>'+
									'<td>'+status+'</td>'+
									'<td></td>'+
									'<td>'+val.updated_at+'</td>'+
									'<td>'+toggle+'</td>'+
									'<td><a href="#myAlert" data-toggle="modal" class="btn update-anouncement-section" value="'+val.id+'"><i class="icon-pencil "></i>Edit</a>'+
								'</tr>';
sno=sno+1;
								

					$('.tableData').html(tabledata);
					$('.announcement').addClass("active");
           
					$('.wait').hide();
			});
		}
		});
});
	
	$("body").on("click",".update-anouncement-section",function(){
		var modal   =null;
		var tableIDnotice='';
		var tableIDnotice =$(this).attr("value");
		console.log(tableIDnotice);
		/*console.log($('.update-anouncement-section').vlaue());*/
		$('.photogallery-upload').hide();
		$.ajax({
			url:'updateannouncementsection',
			type:'GET',
			data:{id:tableIDnotice},
			success:function(responsedata1){
					console.log(responsedata1);
					var contentResponsedata=JSON.parse(responsedata1);
					$.each(contentResponsedata,function(keydata,valdata){
					modaldata= '<div class="modal-header">'+
					'<button data-dismiss="modal" class="close" type="button">&times;</button>'+
						'<input type="hidden" name="id" value="'+tableIDnotice+'">'+
						'<h3><input type="text" name="updateanouncementtitle" value="'+valdata.title+'" /></h3>'+
					'</div>'+
					'<div class="modal-body">'+
						'<p><textarea name="updateanouncementcontent" class="span12">'+valdata.content+'</textarea></p>'+
					'</div><div class="modal-footer">'+
												'<button type="submit" class="btn btn-primary " >Confirm</button>'+
												'<a href="#" class="btn" data-dismiss="modal">Cancel</a>'+
											'</div>';
					});
					$('.displayupdatecommand').hide();
					$('.displayupdatecommand').show().html(modaldata);
				}
			});
	});

	$('body').on('click','.content',function(){
		var modal   =null;
		var tableID =$(this).attr("value");
		$('.photogallery-upload').hide();
		$.ajax({
			url:'contentofannouncement',
			type:'GET',
			data:{id:tableID},
			success:function(responsedata){
					
					var contentResponse=JSON.parse(responsedata);
					$.each(contentResponse,function(key,val){
					modal= '<div class="modal-header">'+
					'<button data-dismiss="modal" class="close" type="button">&times;</button>'+
						'<h3>'+val.title+'</h3>'+
					'</div>'+
					'<div class="modal-body">'+
						'<p>'+val.content+'</p>'+
					'</div>';
					});

					$('#myModal').show().html(modal);
				}
			});
	});
	$('body').on('click','.toggle',function(){
		var tableID=$(this).attr("value");
		var statusTo=$(this).attr("data");
		$('.photogallery-upload').hide();
		$.ajax({

			type:"post",
			data:{status:statusTo,id:tableID},
			url: 'activedeactiverecords',
			success:function(response){

				var jsonData=JSON.parse(response);
				$.each(jsonData,function(key,val){
				console.log(val.success);
				if(val.success){

					 	$.jGrowl("Record Published.",{ sticky: false, theme:'success' });
				}
				else if(val.fail){
					 	$.jGrowl("Some Problem While Publishing Record Please Try After Some Time",{ sticky: false, theme:'fails' });
				}
					
				});				
				
			}
		});

	});

	//*************************************************************************
	//							   Routes
	//*************************************************************************

	$("body").on('click','.notice-board',function(){
			$('.tableData').empty();
			$('.wait').show();
			$('.photogallery').removeClass("active");
			$('.videogallery').removeClass("active");
			$('.announcement').removeClass("active");
			$('.photogallery-upload').hide();
			tabledata='';
		$.ajax({

			type:"GET",
			url:"noticeboard",
			success:function(response){

			var jsonData=JSON.parse(response);
			var sno=1;
			$.each(jsonData,function(key,val){
				if(val.status==1){
					status="<span class='label label-success'>Active</span>";
					toggle='<button class="btn btn-default sm toggle-notice" data="deactiv" value="'+val.id+'">Deactivate</button>';
				}
				else if(val.status==0){
					status="<span class='label label-important'>Deactive</span>";					
					toggle='<button class="btn btn-default sm toggle-notice" data="activ" value="'+val.id+'">Activate</button>';
				}
				 tabledata+='<tr>'+
									'<td>'+sno+'</td>'+
									'<td>'+val.title+'</td>'+
									'<td><a href="#myModal" data-toggle="modal" class="btn btn-primary content-notice" value="'+val.id+'">View</a>'+
									'</td>'+
									'<td>'+status+'</td>'+
									'<td>'+val.mark+'</td>'+
									'<td>'+val.updated_at+'</td>'+
									'<td>'+toggle+'</td>'+
									'<td><a href="#myAlert" data-toggle="modal" class="btn update-content-notice" value="'+val.id+'"><i class="icon-pencil "></i>Edit</a>'+
								'</tr>';
				sno=sno+1;
					$('.tableData').html(tabledata).append();
					

           
					$('.wait').hide();

			});
					
					$('.notice').addClass("active");


					//console.log(response);
			}
		});

	});
	$("body").on("click",".content-notice",function(){
		var modal   =null;
		var tableID =$(this).attr("value");

		$.ajax({
			url:'contentofnotice',
			type:'GET',
			data:{id:tableID},
			success:function(response){
					
					var contentResponse=JSON.parse(response);
					$.each(contentResponse,function(key,val){
					modal= '<div class="modal-header">'+
					'<button data-dismiss="modal" class="close" type="button">&times;</button>'+
						'<h3>'+val.title+'</h3>'+
					'</div>'+
					'<div class="modal-body">'+
						'<p>'+val.content+'</p>'+
					'</div>';
					});

					$('#myModal').show().html(modal);
				}
			});
	});
	$("body").on("click",".update-content-notice",function(){
		var modal   =null;
		var tableID='';
		var tableID =$(this).attr("value");
		$('.photogallery-upload').hide();
		$.ajax({
			url:'updatenoticeBoard',
			type:'GET',
			data:{id:tableID},
			success:function(response){
					
					var contentResponse=JSON.parse(response);
					$.each(contentResponse,function(key,val){
					modal= '<div class="modal-header">'+
									'<button data-dismiss="modal" class="close" type="button">&times;</button>'+
									'<input type="hidden" name="id" value="'+tableID+'">'+
										' <h3><input type="text" name="updatenoticetitle" value="'+val.title+'"></input></h3>'+
					    	'</div>'+
					'<div class="modal-body">'+
						'<p><textarea name="updatenoticecontent" >'+val.content+'</textarea></p>'+
						'<button type="submit" class="btn btn-primary upppp" >Confirm</button>'+
												'<button class="btn" data-dismiss="modal">Cancel</button>'+
											
					'</div>';
					});

					$('.displayupdatecommand').show().html(modal);
				}
			});
	});



	$('body').on('click','.toggle-notice',function(){
		var tableID=$(this).attr("value");
		var statusTo=$(this).attr("data");
		$('.photogallery-upload').hide();
		$.ajax({
			type:"post",
			data:{status:statusTo,id:tableID},
			url: 'activedeactiverecordsfornotice',
			success:function(response){

				var jsonData=JSON.parse(response);
				$.each(jsonData,function(key,val){
				console.log(val.success);
				if(val.success){

					 	$.jGrowl("Record Published.",{ sticky: false, theme:'success' });
				}
				else if(val.fail){
					 	$.jGrowl("Some Problem While Publishing Record Please Try After Some Time.",{ sticky: false, theme:'fail' });
				}
					
				});				
				
			}
		});

	});


	$('body').on('click','.photo-gallery',function(){
		$('.tableData').empty();
			$('.wait').show();
			$('.videogallery').removeClass("active");
			$('.announcement').removeClass("active");
			$('.photogallery-upload').hide();
			tabledata='';
		$.ajax({

			type:"GET",
			url:"photodetails",
			success:function(response){

			var jsonData=JSON.parse(response);
			var sno=1;
			$.each(jsonData,function(key,val){
				if(val.status==1){
					status="<span class='label label-success'>Active</span>";
					toggle='<button class="btn btn-default sm toggle-photo" data="deactiv" value="'+val.id+'">Deactivate</button>';
				}
				else if(val.status==0){
					status="<span class='label label-important'>Deactive</span>";					
					toggle='<button class="btn btn-default sm toggle-photo" data="activ" value="'+val.id+'">Activate</button>';
				}
				 tabledata+='<tr>'+
									'<td>'+sno+'</td>'+
									'<td><img width="300" height="52" src="photogallery/'+val.content+'"></td>'+
									'<td><a href="#myModal" data-toggle="modal" class="btn btn-primary content-photo" value="'+val.id+'">View</a>'+
									'</td>'+
									'<td>'+status+'</td>'+
									'<td></td>'+
									'<td>'+val.updated_at+'</td>'+
									'<td>'+toggle+'</td>'+
									'<td><a href="#modify" class="" value="'+val.id+'"></a>'+
								'</tr>';
				sno=sno+1;
					$('.tableData').html(tabledata).append();
					$('.noticeboard').removeClass("active");
					$('.photogallery').addClass("active");

           
					$('.wait').hide();

			});
					$('.notice').removeClass("active");
					$('.photogallery').addClass("active");
					

					//console.log(response);
			}
		});

	});
$("body").on("click",".content-photo",function(){
			$('.notice').removeClass("active");
			$('.photogallery').removeClass("active");
			$('.announcement').removeClass("active");
			$('.photogallery-upload').hide();
		var modal   =null;
		var tableID =$(this).attr("value");

		$.ajax({
			url:'contentofpicture',
			type:'GET',
			data:{id:tableID},
			success:function(response){
					
					var contentResponse=JSON.parse(response);
					$.each(contentResponse,function(key,val){
					modal= '<div class="modal-header">'+
					'<button data-dismiss="modal" class="close" type="button">&times;</button>'+
						'<h3>'+val.title+'</h3>'+
					'</div>'+
					'<div class="modal-body">'+
						'<p><img src="photogallery/'+val.content+'" alt="'+val.content+'"></p>'+
					'</div>';
					});

					$('#myModal').show().html(modal);
				}
			});
	});
	$('body').on('click','.toggle-photo',function(){
		var tableID=$(this).attr("value");
		var statusTo=$(this).attr("data");
		$('.photogallery-upload').hide();
		
		$.ajax({
			type:"post",
			data:{status:statusTo,id:tableID},
			url: 'activedeactiverecordsforphoto',
			success:function(response){

				var jsonData=JSON.parse(response);
				$.each(jsonData,function(key,val){
				console.log(val.success);
				if(val.success){

					 	$.jGrowl("Record Published.",{ sticky: false, theme:'success' });
				}
				else if(val.fail){
					 	$.jGrowl("Some Problem While Publishing Record Please Try After Some Time.",{ sticky: false, theme:'fail' });
			
				}
					
				});				
				
			}
		});

	});


	$('body').on('click','.video-gallery',function(){
			$('.tableData').empty();
			$('.wait').show();
			$('.notice').removeClass("active");
			$('.photogallery').removeClass("active");
			$('.announcement').removeClass("active");
			$('.photogallery-upload').hide();

			tabledata='';
		$.ajax({

			type:"GET",
			url:"videodetails",
			success:function(response){

			var jsonData=JSON.parse(response);
			var sno=1;
			$.each(jsonData,function(key,val){
				if(val.status==1){
					status="<span class='label label-success'>Active</span>";
					toggle='<button class="btn btn-default sm toggle-video" data="deactiv" value="'+val.id+'">Deactivate</button>';
				}
				else if(val.status==0){
					status="<span class='label label-important'>Deactive</span>";					
					toggle='<button class="btn btn-default sm toggle-video" data="activ" value="'+val.id+'">Activate</button>';
				}
				 tabledata+='<tr>'+
									'<td>'+sno+'</td>'+
									'<td>'+val.title+'</td>'+
									'<td><a href="#myModal" data-toggle="modal" class="btn btn-primary content-video" value="'+val.id+'">View</a>'+
									'</td>'+
									'<td>'+status+'</td>'+
									'<td></td>'+
									'<td>'+val.updated_at+'</td>'+
									'<td>'+toggle+'</td>'+
									'<td><a href="#modify" class="btn " value="'+val.id+'"><i class="icon-pencil "></i>Edit</a>'+
								'</tr>';
				sno=sno+1;
					$('.tableData').html(tabledata).append();
					
					$('.videogallery').addClass("active");

           
					$('.wait').hide();

			});
								//console.log(response);
			}
		});

	});
$("body").on("click",".content-video",function(){
			$('.notice').removeClass("active");
			$('.photogallery').removeClass("active");
			$('.announcement').removeClass("active");
			$('.photogallery-upload').hide();
		var modal   =null;
		var tableID =$(this).attr("value");

		$.ajax({
			url:'contentofvideo',
			type:'GET',
			data:{id:tableID},
			success:function(response){
					
					var contentResponse=JSON.parse(response);
					$.each(contentResponse,function(key,val){
					modal= '<div class="modal-header">'+
					'<button data-dismiss="modal" class="close" type="button">&times;</button>'+
						'<h3>'+val.title+'</h3>'+
					'</div>'+
					'<div class="modal-body">'+
						'<p>'+val.content+'</p>'+
					'</div>';
					});

					$('#myModal').show().html(modal);
				}
			});
	});
	$('body').on('click','.toggle-video',function(){
		var tableID=$(this).attr("value");
		var statusTo=$(this).attr("data");
		$('.photogallery-upload').hide();
		
		$.ajax({
			type:"post",
			data:{status:statusTo,id:tableID},
			url: 'activedeactiverecordsforvideo',
			success:function(response){

				var jsonData=JSON.parse(response);
				$.each(jsonData,function(key,val){
				console.log(val.success);
				if(val.success){

					//$.jGrowl(val.success);
					$.jGrowl(val.success,{ sticky: false, theme:'success' });
			
				}
				else if(val.fail){
			 	$.jGrowl(val.fail,{ sticky: false, theme:'fail' });
			
				}
					
				});				
				
			}
		});

	});

	var formUi='';

	$("body").on("click",".anouncement-section",function(){
		var tableID='';
		   tableID =$(this).attr("value");
		var formUi='<input type="hidden" name="id" value="'+tableID+'">'+
		            '<input type="text" class="span4 m-wrap" data-required="1" name="announcementtitle" placeholder="Title..">'+
                    '<textarea class="span4" name="announcementcontent" placeholder="Your Content To Display.."></textarea>'+
                    '<br/><button type="submit" class="btn btn-success">Publish</button>';
                   $('.form-ui').show();
                   $(".form-ui").html(formUi);

	});
	$("body").on("click",".notice-board",function(){
			var tableID='';
		   tableID =$(this).attr("value");
			var formUi='<input type="hidden" name="id" value="'+tableID+'">'+
			           '<input type="text" name="noticetitle" placeholder="Title of notice.." />'+
			           '<textarea class="span4" name="noticecontent" placeholder="Your Content To Display.."></textarea>'+
                    '<input type="checkbox" value="new" id="optionsCheckbox" class="uniform_on" name="noticemarknew">Mark New'+
                    '<br/><button type="submit" class="btn btn-success">Publish</button>';
                   $('.form-ui').show();
                   $(".form-ui").html(formUi);

	});
	$("body").on("click",".photo-gallery",function(){

		$formUi='';
		var formUi='<span>File</span>'+
						'<input type="file" id="file" name="file" size="10"/>'+
						'<input id="uploadbutton" type="button" value="Upload"/>'+
							'<button type="submit" >Submit</button>';
							$('.form-ui').hide();
							$('.photogallery-upload').show();
							   $(".form-pic").html(formUi);


	});
	$('.form-ui').show();
	$("body").on("click",".video-gallery",function(){
		$formUi='';
		var formUi='<input type="text" class="span4 m-wrap" data-required="1" name="videotitle" placeholder="Title..">'+
                    '<textarea name="videocontent" class="span4" placeholder="url of video..."></textarea>'+
                    '<br/><button type="submit" class="btn btn-success">Publish</button>';
                   $('.form-ui').show();
                   $(".form-ui").html(formUi);


	});

	

    $("#multiform").submit(function() {
    	$('.photogallery-upload').hide();
    var url = "postdata"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#multiform").serialize(), // serializes the form's elements.
           success: function(data)
           {
               if(data=="fail"){
    //           	$.jGrowl("Some Error While Publishing Record.");
    	 	$.jGrowl("Some Problem While Publishing Record Please Try After Some Time.",{ sticky: false, theme:'success' });
	
               } // show response from the php script.
               else if(data=="success"){
            
               	$.jGrowl("Record Published.",{ sticky: false, theme:'fail' });
	
               }
               else{
               	$.jGrowl("We Found Some Problem, Contact Support.");
               }
           }
         });

    return false; // avoid to execute the actual submit of the form.
});

$("#myalertform").submit(function(){
	$('.photogallery-upload').hide();
    	/*console.log('hi');*/
    var url = "updatedata"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#myalertform").serialize(), // serializes the form's elements.
           success: function(data)
           {
           	console.log(data);
               if(data=="fail"){
               	$.jGrowl("Some Error While Publishing Record.",{ sticky: false, theme:'fail' });
               } // show response from the php script.
               else if(data=="success"){
               	$.jGrowl("Record Published.",{ sticky: false, theme:'success' });
               }
               else{
               	$.jGrowl("We Found Some Problem, Contact Support.",{ sticky: false, theme:'fail' });
               }
           }
         });

    return false; // avoid to execute the actual submit of the form.
});



$('.pic-upload').submit(function(){
   $('.photogallery-upload').hide();


    var formData = new FormData($('form')[0]);
    $.ajax({
        url: 'uploadfile',  //Server script to process data
        type: 'POST',
        data: formData,

        xhr: function() {  // Custom XMLHttpRequest
            var myXhr = $.ajaxSettings.xhr();
            if(myXhr.upload){ // Check if upload property exists
                myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
            }

            return myXhr;

        },
        //Ajax events
       
        success: function(response){

            console.log(response);
          	$.jGrowl("Uploaded.");
        },
        error: function(response){
             console.log(response);
             //alert('hi');
        },
        // Form data
        
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false
    });
});
   function progressHandlingFunction(e){
    if(e.lengthComputable){

        $('progress').attr({value:e.loaded,max:e.total}).css({'display':'block'});
    }
}

});