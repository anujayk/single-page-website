<?php
echo ' <footer>
          <div class="main-footer">
            <div class="row">
              <div class="large-5 medium-3 columns">
                <div class="list">
                  <h5>Contact us</h5>
                  <p>Address -   6/112 Sec-2, Rajender Nagar, Sahibabad, Near DAV School, U.P.
                    <br>
                    Contact - 9873341370 or 9312922770
                     <br>
                    Email – <a href="mailto:Greycells.info@gmail.com">Greycells.info@gmail.com</a></p>
                </div>                
              </div>

              <div class="large-2 medium-3 columns">
                <div class="list">
                  <h5>Links</h5>
                  <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="index.php#faculty">Faculty</a></li>
                    <li><a href="">Courses</a></li>
                    <li><a href="careers.php">Careers</a></li>
                    <li><a href="video.php">Video</a></li>
                  </ul>
                </div>                
              </div>

              <div class="large-2 medium-3 columns">
                <div class="list">
                  <h5>Links</h5>
                  <ul>
                    <li><a href="gallery.php">Gallery</a></li>
                    <li><a href="result.php">Results</a></li>
                    <li><a href="school-partnerships.php">School Partnerships</a></li>
                    <li><a href="about.php">About us</a></li>
                    <li><a href="contact.php">Contact us</a></li>
                  </ul>
                </div>
              </div>

              <div class="large-3 medium-3 columns">
                <div class="list">
                  <h5>Photo Gallery</h5>
                  <ul class="gallery clearfix">
                    <li><a href="gallery.php"><img src="img/gallery/1.JPG"></a></li>
                    <li><a href="gallery.php"><img src="img/gallery/2.JPG"></a></li>
                    <li><a href="gallery.php"><img src="img/gallery/3.JPG"></a></li>
                    <li><a href="gallery.php"><img src="img/gallery/4.JPG"></a></li>
                    <li><a href="gallery.php"><img src="img/gallery/5.JPG"></a></li>
                    <li><a href="gallery.php"><img src="img/gallery/6.JPG"></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="footer-bottom">
            <div class="row">
              <div class="large-12 columns">
                <div class="list">
                  <p>GreyCells Learning Private limited – copyright</p>
                </div>
              </div>
            </div>
          </div>
        </footer>';
?>