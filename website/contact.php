<!doctype html>
<html class="no-js" lang="en">
 <?php
  include'head.php'
  ?>
  <body>
    
    <div class="main-container">
      <div class="container">
        <div class="header">
          <div class="row">
            <div class="large-12 columns">
              <div class="logo">
                <a href="index.php"><img src="img/logo.jpg" style="width:262px;"></a>
              </div>
            </div>

            <?php
              include 'nav.php';
              ?>
          </div>
        </div>

        <div class="content-container">
        	<div class="row">
        		<div class="large-12 medium-12 columns">
        			<div class="content">
        				<div class="heading">
        					<h2><span>Contact us</span></h2>
        				</div>

        				<div class="text">
        					<p>Address -   6/112 Sec-2, Rajender Nagar, Sahibabad, Near DAV School, U.P.
		                    <br>
		                    Contact - 9873341370 or 9312922770
		                     <br>
		                    Email - <a href="mailto:Greycells.info@gmail.com">Greycells.info@gmail.com</a></a></p>
        				</div>

        				<div class="map">
						  	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3500.0434109213147!2d77.35323129999999!3d28.68834799999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfa5dd09bf7f5%3A0xe7ae3f5d6ffe4c6d!2s3%2F112!5e0!3m2!1sen!2sin!4v1400329896106" width="100%" height="450" frameborder="0" style="border:0"></iframe>
						</div>

        			</div>
        		</div>
        	</div>
        </div>
        
          <?php
        include'footer.php';
       ?>
      </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
