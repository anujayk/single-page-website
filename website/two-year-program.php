<!doctype html>
<html class="no-js" lang="en">
    <?php
        include'head.php';
       ?>
  <body>
    
    <div class="main-container">
      <div class="container">
        <div class="header">
          <div class="row">
            <div class="large-12 columns">
              <div class="logo">
                <a href="index.php"><img src="img/logo.jpg" style="width:262px;"></a>
              </div>
            </div>

              <?php
        include'nav.php';
       ?>
          </div>
        </div>

        <div class="content-container">
        	<div class="row">
        		<div class="large-8 medium-8 columns">
        			<div class="content">
        				<div class="heading">
        					<h2 class=""><span>TWO YEAR INTEGRATED PROGRAM FOR IITJEE AND CBSE (CLASS 11th and 12th)</span></h2>
        				</div>

        				<div class="text">
        					<h6>Introduction to Program</h6>
        					<p>The two year integrated program at GreyCells’ is designed to simultaneously cover CBSE and IITJEE/AIEEE preparation. We believe that CBSE is a subset of IITJEE preparation and excelling both in IITJEE and CBSE along with hard work requires proper guidance, time management and course planning.</p>
        					<p>Our Course is designed to achieve timely milestones in a student’s preparation so that his school performance is not affected due to the focus on competitive examinations.</p>

        					<table class="discription">
				          		<tbody>
				          			<tr>
				          				<td>Target</td>
				          				<td>-</td>
				          				<td>IITJEE – mains and advance, UPTECH, NDA &amp; CBSE</td>
				          			</tr>
				          			<tr>
				          				<td>Eligibility</td>
				          				<td>-</td>
				          				<td>For XI undergoing students</td>
				          			</tr>
				          			<tr>
				          				<td>Course Duration</td>
				          				<td>-</td>
				          				<td>Two Year</td>
				          			</tr>
				          			<tr>
				          				<td>Course Commencement</td>
				          				<td>-</td>
				          				<td>April 2014</td>
				          			</tr>
				          			<tr>
				          				<td>Course End</td>
				          				<td>-</td>
				          				<td>May 2016</td>
				          			</tr>
				          			<tr>
				          				<td>Mode of Admission</td>
				          				<td>-</td>
				          				<td>Through an Admission Test which is held on specific Sundays.</td>
				          			</tr>
				          			<tr>
				          				<td>Admission Test Pattern</td>
				          				<td>-</td>
				          				<td>Objective Type - Sample Paper available on the website.</td>
				          			</tr>
				          			<tr>
				          				<td>Admission Test Syllabus</td>
				          				<td>-</td>
				          				<td>Class VIII, IX and X syllabus of Physics, Chemistry &amp; Maths</td>
				          			</tr>
				          			<tr>
				          				<td style="vertical-align:top;">Features of the program</td>
				          				<td style="vertical-align:top;">-</td>
				          				<td>
				          					<ul>
				          						<li>All IIT faculty</li>
				          						<li>Complete coverage of CBSE syllabus</li>
				          						<li>Stress on fundamentals.</li>
				          						<li>Separate Doubt Discussion sessions</li>
				          						<li>Regular Counseling sessions by IITians (IITians Live)</li>
				          						<li>Fortnightly test</li>
				          						<li>Cab facility available</li>
				          					</ul>
				      					</td>
				          			</tr>
				          			<tr>
				          				<td>Objective</td>
				          				<td>-</td>
				          				<td>To Excel both in competition and CBSE</td>
				          			</tr>
				          			<tr>
				          				<td>Evening Batch Timings</td>
				          				<td>-</td>
				          				<td>4:30 - 8:00 pm</td>
				          			</tr>
				          			<tr>
				          				<td>Number of days per week</td>
				          				<td>-</td>
				          				<td>3 Lecture days, Test every alternative week</td>
				          			</tr>
				          			<tr>
				          				<td>Committed hours per week </td>
				          				<td>-</td>
				          				<td>12 Hrs</td>
				          			</tr>
				          			<tr>
				          				<td style="vertical-align:top;">Registration requirement</td>
				          				<td style="vertical-align:top;">-</td>
				          				<td>
				          					<ul>
				          						<li>Filled registration form</li>
				          						<li>2 passport size photos</li>
				          						<li>10th or 9th report card photocopy</li>
				          						<li>Registration fees (part of the total fees)</li>
				          					</ul>
				      					</td>
				          			</tr>
				          		</tbody>
				          	</table>

				          	<h6>FAQs of the Program</h6>
				          	<blockquote>
				          		<h5>Question 1: Does GreyCells provide Scholarship to Students?</h5> 
								<p><b>Answer:</b> GreyCells awards scholarship to meritorious students solely on the basis of their performance in the admission/scholarship test held at the center on specific Sundays.
								Admission may also be extended without any scholarship if the student does not qualify in any of the scholarship categories. Students whose performance is below a certain cutoff marks will not be given admission.</p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Question 2: What is the tentative timetable of the program?</h5>
				          		<p><b>Answer:</b> Classes are held on 3 days a week for eg Monday, Wednesday and Friday. Classes are held in the evening time from 4:30 pm to 8:00 pm. Students are also called on Sundays for extra class and weekly tests. The days of the program vary from batch to batch. During summer holidays and school breaks, students are called in addition to the above timetable to cover the course. <b>In case of doubts or miss of lecture, extra sessions are held on Sundays.</b></p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Question 3: What is the test schedule and how is feedback given to parents.</h5>
				          		<p><b>Answer:</b> Tests are held on fortnightly basis on alternative Sundays (exact timetable will be given at the commencement of the program). Tests are generally held in the first half of the day on alternative Sundays tentatively from 11AM to 2PM.<br> Each test is of the pattern of IITJEE Mains (objective). There are 3 sections one of Physics, Chemistry and Maths. Each section has a total of 30 multiple choice questions, each having four options to select the right answer from. Making scheme is as per competitive exams – 4 for right answer and -1 for wrong answer. The test results are displayed on the center’s notice board within 2-3 working days with marks and ranks of the students. Parents can also enquire the performance</p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Question 4: For which places is cab facility provided.</h5>
				          		<p><b>Answer:</b> Cab facility is provided from MMX Mall, Air Force Station Hindon Gate, Shalimar Garden (Shiv Chowk and 80 ft Rd), Rajender Nagar Sec-3, Sec-2 and Sec-5, Shyam Park Extn. Jindal Market, Mohan Nagar (Parsvnath), Lajpat Nagar. Additional locations are added if there are at least 3 students from the locality. The monthly charges for a location can be inquired at the center. Payment of cab fees has to be done on quarterly basis in advance.</p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Questions 5:  What are the fees installments?</h5>
				          		<p><b>Answers:</b> The fees is payable in 3 installments: <br> <b>Ist Installment</b> – At Admission (before April 2014), <b>Second Installment</b> – 6 months from admission (September 2014), <b>Third Installment</b> – 1 year after admission (March 2015). All scholarship is adjusted in the third installment. There is an additional service tax of 12.3% on each payment. The payment has to be issued in the name of ‘Greycells Learning Pvt Limited’. Additional discount is provided if full payment of the program is done in first installment.</p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Questions 6:  Will School CBSE syllabus be covered?</h5>
				          		<p><b>Answer:</b> CBSE preparation is subset of competitive syllabus. <b>Full CBSE syllabus is covered for each topic and there is no need of a student to join extra tuition.</b> In case there are extra doubts related to school syllabus students are called extra on Sundays.</p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Questions 7:  Are classes held in summer vacation?</h5>
				          		<p><b>Answer:</b> Classes are held during summer vacations (i.e during April, May , June) hence there are no summer vacations at GreyCells. Classes during summer vacation help to cover a lot of course before school starts, hence the student is comfortable when the school starts in July. </p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Questions 8:  When are the holidays given in the program?</h5>
				          		<p><b>Answer:</b> There are no major holidays, but vacations are given during Diwali and New year. Also there are no classes in March as there are school examinations at school.</p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Questions 9:  What study material is given in the program?</h5>
				          		<p><b>Answer:</b> Complete study package is given in form of booklets and weekly assignments for each topic. These booklets and assignment cover theory, CBSE level questions, competitive Questions and previous year questions. <br><b>However, few books are a must for each student to buy for the success of the program as they are referred in the classes: </b> <br>NCERT books <br>R.D Sharma (math) <br>Pradeep (physics and chemistry) <br>Concept of Physics by H.C Verma (available at center at discounted rates) <br> Mathematics for AIEEE by Prafull K Aggarwal – Arihant publication (available at center at discounted rates) <br>Chemical Calculations – RC Mukerjee (available at center at discounted rates)</p>
				          	</blockquote>
        				</div>
        			</div>
        		</div>

        		<div class="large-4 medium-12 columns left-main">
        			<div class="">
		                <div class="heading">
		                  <h2><span>GreyCells on facbook</span></h2>
		                </div>

		                <div class="fb-feeds">
		                  <img src="img/fb.png">
		                </div>

		                <div class="video-container">
		                  <div class="heading">
		                    <h2><span>virtual tour of GreyCells Center</span></h2>
		                  </div>

		                  <div class="video">
		                    <iframe width="100%" height="250" src="//www.youtube.com/embed/Hh6B362ffbk" frameborder="0" allowfullscreen=""></iframe>
		                  </div>
		                </div>
	                </div>
        		</div>
        	</div>
        </div>
        
          <?php
        include'footer.php';
       ?>
      </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
