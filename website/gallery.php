<!doctype html>
<php lang="en">
  <?php 
  include 'config.php';
  include'head.php';
  ?>
  <body>
    
    <div class="main-container">
      <div class="container">
        <div class="header">
          <div class="row">
            <div class="large-12 columns">
              <div class="logo">
                <a href="index.php"><img src="img/logo.jpg" style="width:262px;"></a>
              </div>
            </div>
            	<?php
            	include 'nav.php';
            	?>
          </div>
        </div>

        <div class="content-container">
        	<div class="row">
        		<div class="large-8 medium-8 columns">
        			<div class="content">
        				<div class="heading">
        					<h2><span>Gallery</span></h2>
        				</div>

        				<div class="slider">
			                <ul class="example-orbit" data-orbit>

			                <?php 
			                $sql="select * from tbl_photo_gallery where status='1' order By id DESC";
			                $result=mysql_query($sql);
                    

                      if(mysql_num_rows($result)){
			                while($row=mysql_fetch_assoc($result)){
			                		echo '<li>
			                    <img src="../admin/public/photogallery/'.$row['content'].'"/>
			                  </li>';
			                }}
                      else{
                        echo 'Nothing To Display';
                      }
			               	 ?>
                      
			                  
			                </ul>
			              </div>
        			</div>
        		</div>

        		<div class="large-4 medium-12 columns left-main">
        			<div class="">
		                <div class="heading">
		                  <h2><span>GreyCells on facbook</span></h2>
		                </div>

		                <div class="fb-feeds">
		                  <img src="img/fb.png">
		                </div>

		                <div class="video-container">
		                  <div class="heading">
		                    <h2><span>virtual tour of GreyCells Center</span></h2>
		                  </div>

		                  <div class="video">
		                    <iframe width="100%" height="250" src="//www.youtube.com/embed/Hh6B362ffbk" frameborder="0" allowfullscreen=""></iframe>
		                  </div>
		                </div>
	                </div>
        		</div>
        	</div>
        </div>
        
       <?php
       	include'footer.php';
       ?>
      </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</php>
