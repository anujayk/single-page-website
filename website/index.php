<!doctype html>
<html class="no-js" lang="en">
   <?php
        include 'config.php';
        include'head.php';
       ?>
  <body>
    
    <div class="main-container">
      <div class="container">
        <div class="header">
          <div class="row">
            <div class="large-6 medium-6 columns">
              <div class="logo">
                <a href="index.php"><img src="img/logo.jpg" style="width:400px;"></a>
              </div>
            </div>

            <div class='large-6 medium-6 columns'>
              <form class="query-form">
                <span class="button main show-button">Send a query</span>
                <div class="form-text">
                  <div class="row">
                    <div class="large-12 columns">
                      <label>Name
                        <input type="text" name="name" placeholder="Enter your name" />
                      </label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <label>Mobile
                        <input type="text" name="phone" placeholder="Enter your mobile No." />
                      </label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <label>Email
                        <input type="text" name="email" placeholder="Enter your email id" />
                      </label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <label>Query
                        <textarea name="detail" placeholder="Enter your query"></textarea>
                      </label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <label>
                        <button class="button tiny">Submit</button>
                      </label>
                    </div>
                  </div>
                  <span class="email-success"</span>
                </div>
              </form>
            </div>

           <?php
        include'nav.php';
       ?>
          </div>
        </div>

        <div class="slider-part">
          <div class="carousel">
            <div class="row">
              <div class="large-4 medium-4 columns padding-right">
                <div class="slide-caption comp-a">
                  <h2>Announcements</h2>
                  <div class="marquee-container">
                    <marquee behavior="scroll" direction="up" onmouseover="this.stop();" onmouseout="this.start();" scrollamount="1">
                      <div class="detail">
                        <ol>
                        <?php
                          $sql="select * from tbl_announcement where status='1' order By id DESC";
                          $query=mysql_query($sql);
                          while($row=mysql_fetch_assoc($query)){
                          echo'<li><a href="#">'.$row['content'].'</a></li>';
                         
                          }
                        ?>
                        </ol>
                      </div>
                    </marquee>
                  </div>
                </div><!-- end slide-caption  comp-a -->
              </div>
              
              <div class="large-8 medium-8 columns padding-left">
                <div class="carousel-image">
                  <ul class="example-orbit" data-orbit>
                    <li>
                      <img src="img/CBSE-Board-Analysis.jpg" alt="slide 1" />
                    </li>
                    <li class="active">
                      <img src="img/Result-Analysis-of-Selection.jpg" alt="slide 2" />
                    </li>
                    <li>
                      <img src="img/Imgae-1.jpg" alt="slide 3" />
                    </li>
                    <li>
                      <img src="img/Result-Analysis-of-Selection.jpg" alt="slide 4" />
                    </li>
                    <li class="active">
                      <img src="img/CBSE-Board-Analysis.jpg" alt="slide 5" />
                    </li>
                    <li>
                      <img src="img/Imgae-1.jpg" alt="slide 6" />
                    </li>
                  </ul>
                </div><!-- end carousel-image -->
              </div>
            </div>
          </div>
        </div>

        <div class="courses">
          <div class="row">
            <div class="large-12 columns">
              <div class="large-4 medium-4 columns list">
                <div class="courses-detail">
                  <a href="two-year-program.php"><span>Class 11th and 12th</span> 2 year program for IITJEE and CBSE</a>
                </div>  
              </div>

              <div class="large-4 medium-4 columns list">
                <div class="courses-detail">
                  <a href="one-year-program.php"><span>Class 12th and Drop outs</span> 1 year program for IITJEE and CBSE</a>
                </div>  
              </div>

              <div class="large-4 medium-4 columns list">
                <div class="courses-detail">
                  <a href="Foundation-program.php"><span>Class 9th and 10</span> Foundation program for Class 9th and 10th</a>
                </div>  
              </div>
            </div>
          </div>
        </div>

        <div class="main-container">
          <div class="row">
            <div class="large-8 medium-12 columns">
              <div class="faculty clearfix" id="faculty">
                <div class="large-12 medium-12 columns">
                  <div class="heading">
                    <h2><span>Faculty on greycells</span></h2>
                  </div>
                </div>

                <div class="large-6 medium-6 columns">
                  <div class="faculty-detail">
                    <div class="faculty-head">
                      <a href="" data-reveal-id="firstModal"><h4>RAJAT SOOD – ( PHYSICS )</h4></a>
                    </div>

                    <div class="faculty-data">
                      <div class="faculty-img">
                        <a href="" data-reveal-id="firstModal"><img src="img/rajat sood.jpg"></a>
                      </div>

                      <div class="faculty-text">
                        <p>Rajat did a dual degree (B.Tech &amp; M.Tech) in Chemical Engineering from IIT-Delhi in 2005...</p>
                        <a href="" data-reveal-id="firstModal">Read more...</a>
                      </div>
                    </div>               
                  </div>

                  <div id="firstModal" class="reveal-modal padding-none medium" data-reveal>  <!-- reveal start -->
                    <h2>Rajat Sood – Physics (B.Tech and M.Tech, IIT-Delhi)</h2>
                    <div class="inner-modal clearfix">
                      <div class="image-part">
                        <img src="img/rajat sood.jpg">
                      </div>

                      <div class="text-part">
                        <p>Rajat did a dual degree (B.Tech &amp; M.Tech) in Chemical Engineering from IIT-Delhi in 2005. He did summer internships at various world-class universities in Germany, Netherlands and Singapore during his studies at IIT-Delhi. Physics was his favorite subject right from his IITJEE preparation days. While at IIT-Delhi, he mentored many class XI &amp; XII students in Physics. He relates most of the concept with real life situations which makes even the toughest of the concepts extremely easy and makes Physics interesting and enjoyable. He also likes Physical Chemistry and takes classes of some topics of Physical Chemistry @ GreyCells. He also takes care of strategy and marketing at GreyCells. </p>
                        <p>Before starting GreyCells, Rajat worked with a research and consulting firm - Evalueserve @ Gurgaon for three years.</p>
                      </div>
                    </div>
                    <a class="close-reveal-modal close1">&#215;</a>
                  </div> <!-- reveal end -->
                </div>

                <div class="large-6 medium-6 columns">
                  <div class="faculty-detail">
                    <div class="faculty-head">
                      <a href="" data-reveal-id="secondModal"><h4>JEETENDRA SINGH - ( CHEMISTRY )</h4></a>
                    </div>

                    <div class="faculty-data">
                      <div class="faculty-img">
                        <a href="" data-reveal-id="secondModal"><img src="img/Jeetendra Singh.jpg"></a>
                      </div>

                      <div class="faculty-text">
                        <p>Jeetendra did his M.Tech in Chemical analysis from IIT-Delhi in 2008...</p>
                        <a href="" data-reveal-id="secondModal">Read more...</a>
                      </div>
                    </div>
                  </div>

                  <div id="secondModal" class="reveal-modal padding-none medium" data-reveal>  <!-- reveal start -->
                    <h2>Jeetendra Singh - Chemistry (M.Tech, IIT-Delhi)</h2>
                    <div class="inner-modal clearfix">
                      <div class="image-part">
                        <img src="img/Jeetendra Singh.jpg">
                      </div>

                      <div class="text-part">
                        <p>Jeetendra did his M.Tech in Chemical analysis from IIT-Delhi in 2008. Prior to it he has done his M.Sc in Analytical chemistry from IIT-Delhi.  He has published numerous research papers on biophysical data and numerical analysis in Chemistry.</p>
                        <p>His ability to explain complex organic and inorganic concepts in detailed and simplified form makes learning chemistry interesting and easier to remember.   He also heads the planning wing of chemistry content @ GreyCells. He has been mentoring students for IITJEE since past 5 years.</p>
                      </div>
                    </div>
                    <a class="close-reveal-modal close1">&#215;</a>
                  </div> <!-- reveal end -->
                </div>

                <div class="large-6 medium-6 columns">
                  <div class="faculty-detail">
                    <div class="faculty-head">
                      <a href="" data-reveal-id="thirdModal"><h4>ABHISHEK JAISWAL - ( MATHS )</h4></a>
                    </div>

                    <div class="faculty-data">
                      <div class="faculty-img">
                        <a href="" data-reveal-id="thirdModal"><img src="img/abhishek.jpg"></a>
                      </div>

                      <div class="faculty-text">
                        <p>Abhishek did his B.Tech degree in Civil Engineering from IIT Roorkee in 2005...</p>
                        <a href="" data-reveal-id="thirdModal">Read more...</a>
                      </div>                
                    </div>
                  </div>

                  <div id="thirdModal" class="reveal-modal padding-none medium" data-reveal>  <!-- reveal start -->
                    <h2>Abhishek Jaiswal - Maths (B.Tech, IIT-Roorkee)</h2>
                    <div class="inner-modal clearfix">
                      <div class="image-part">
                        <img src="img/abhishek.jpg">
                      </div>

                      <div class="text-part">
                        <p>Abhishek did his B.Tech degree in Civil Engineering from IIT Roorkee in 2005. He did his summer training from Larson and Tubro and was the president of Student Association Council (SAC) of IIT - Roorkee in the year 2004-05.</p>
                        <p>Maths was his forte from his own IITJEE preparation days. He has mentored students for IITJEE and CAT since last 4-5 years.</p>
                        <p>He includes multi conceptual approach in his classes which strengthen students conceptually and enable them to solve tough and complex problems by visualizing them in multiple ways. This technique is very instrumental in attempting IITJEE mathematics.</p>
                        <p>Abhishek has also worked as a business analyst in a research and consulting firm – Evalueserve @ Gurgaon for two years and prior to it he has worked as a software analyst for Tata Consultancy Services (TCS).</p>
                      </div>
                    </div>
                    <a class="close-reveal-modal close1">&#215;</a>
                  </div> <!-- reveal end -->
                </div>

                <div class="large-6 medium-6 columns">
                  
                </div>
              </div>

              <div class="about clearfix">
                <div class="large-12 medium-12 columns">
                  <div class="heading">
                    <h2><span>about GreyCells</span></h2>
                  </div>

                  <div class="text">
                    <p>GreyCells is an institute started by IITians in 2009 with a vision of providing best quality
                      coaching for IITJEE / AIEEE /CBSE in Sahibabad. We are a team of IITians having a common 
                      passion- To Impart High Quality Education. We realized that Sahibabad did not have any reliable coaching institute for IITJEE / AIEEE / UPTECH / IP.</p>  
                    <p>With the changed pattern of IITJEE, we at GreyCells believe that the future belongs to those who concentrate on IITJEE preparation along with the CBSE syllabus.</p>
                    <p>Presently we have evening batches running at our Rajender Nagar center, which comprises 
                      of students from adjoining schools like K.V-1, K.V-2, DAAV, DLF, Airforce hindon, Airforce 
                      Balbharti, Vivekanand(delhi), DPS etc.</p>
                    <p>Also we have a school project running successfully for the last 3 years with DLF public school in which a separate section is created (dedicated for students targeting both IITJEE and CBSE simultaneously) and all the Physics, Chemistry and Maths classes are taken up by GreyCells Mentors.</p>
                    <p>Previously students aspiring for engineering exams had to go to Delhi / Kota to get coaching. This results in wastage of precious time, energy and money of the students and parents. With GreyCells around the corner, now you do not have to go to a far off place to get the expert guidance. The best coaching for IITJEE / AIEEE is now available @ Sahibabad.</p>
                    <a href="about.php">Read more...</a>
                  </div>

                  <img src="img/CBSE-Result 2.jpg">
                </div>                
              </div>
            </div>

            <div class="large-4 medium-12 columns padding-left">
              <div class="left-main">
                <div class="heading">
                  <h2><span>admission open</span></h2>
                </div>

                <div class="video">
                  <img src="img/Admission-Open.jpg">
                </div>
                
                <div class="heading">
                  <h2><span>GreyCells on facbook</span></h2>
                </div>

                <div class="fb-feeds">
                  <img src="img/fb.png">
                </div>

                <div class="video-container">
                  <div class="heading">
                    <h2><span>virtual tour of GreyCells Center</span></h2>
                  </div>

                  <div class="video">
                    <iframe width="100%" height="315" src="//www.youtube.com/embed/1lBUhBmi5HQ?list=UUHTavI0aE6MfKabqh0JxcZw" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="testimonial">
          <div class="row">
            <div class="large-12 columns">
              <div class="slider">
                <ul class="example-orbit" data-orbit>
                  <li>
                    <img src="img/Testimonials-1.jpg"/>
                  </li>

                  <li>
                    <img src="img/Testimonials-2.jpg"/>
                  </li>

                  <li>
                    <img src="img/Testimonials-3.jpg"/>
                  </li>

                  <li>
                    <img src="img/Testimonials-4.jpg"/>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

         <?php
        include'footer.php';
       ?>
      </div>
    </div>
    
    <div id="fourthModal" class="reveal-modal medium" data-reveal>
<a class="close-reveal-modal">&#215;</a>
      <img src="img/unnamed.jpg">
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script

    <script src="js/vendor/jquery.js"></script>

    <script src="js/foundation.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
      $(document).foundation();
    </script>

    <script>




      $(document).ready(function(){
        $("#fourthModal").foundation('reveal', 'open');
      });

      $(function(){
        $('.query-form').submit(function(){

          $.ajax({
            type:"POST",
            url:"email.php",
            data:$('.query-form').serialize(),
            success:function(response){
                   alert('need ur domain hosted email');
            
            }
        });
          return false;

        });
      });
    </script>
  </body>
</html>
