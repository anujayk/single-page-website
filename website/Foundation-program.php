<!doctype html>
<html class="no-js" lang="en">
  <?php
  include'head.php'
  ?>
  <body>
    
    <div class="main-container">
      <div class="container">
        <div class="header">
          <div class="row">
            <div class="large-12 columns">
              <div class="logo">
                 <a href="index.php"><img src="img/logo.jpg" style="width:262px;"></a>
              </div>
            </div>

            <?php
              include 'nav.php';
              ?>
          </div>
        </div>

        <div class="content-container">
        	<div class="row">
        		<div class="large-8 medium-8 columns">
        			<div class="content">
        				<div class="heading">
        					<h2 class=""><span>FOUNDATION PROGRAM (For Class IX &amp; X)</span></h2>
        				</div>

        				<div class="text">
        					<h6>Introduction to Program</h6>
        					<p>Covers all aspects of All Boards (Maths &amp; Science) – Focus on Concepts</p>

				          	<table class="discription">
				          		<tbody>
				          			<tr>
				          				<td>Target</td>
				          				<td>-</td>
				          				<td>CBSE - Maths &amp; Science + Bridging the gap between 10th and 11th</td>
				          			</tr>
				          			<tr>
				          				<td>Eligibility</td>
				          				<td>-</td>
				          				<td>For IX/X </td>
				          			</tr>
				          			<tr>
				          				<td>Course Duration</td>
				          				<td>-</td>
				          				<td>One Year</td>
				          			</tr>
				          			<tr>
				          				<td>Course Commencement</td>
				          				<td>-</td>
				          				<td>April, May 2014</td>
				          			</tr>
				          			<tr>
				          				<td>Course End</td>
				          				<td>-</td>
				          				<td>February 2015</td>
				          			</tr>
				          			<tr>
				          				<td>Mode of Admission</td>
				          				<td>-</td>
				          				<td>Direct admission on the basis of class VIII and IX result.</td>
				          			</tr>
				          			<tr>
				          				<td style="vertical-align:top;">Features of the program</td>
				          				<td style="vertical-align:top;">-</td>
				          				<td>
				          					<ul>
				          						<li>All IIT faculty</li>
				          						<li>Stress on fundamentals.</li>
				          						<li>Separate Doubt Discussion sessions</li>
				          						<li>Regular Counseling sessions by IITians (IITians Live)</li>
				          						<li>Fortnightly test</li>
				          					</ul>
				      					</td>
				          			</tr>
				          			<tr>
				          				<td>Batch Timings</td>
				          				<td>-</td>
				          				<td>Between 4:00 - 6:00 pm on weekdays and 10:00 – 12:00 am on Sundays</td>
				          			</tr>
				          			<tr>
				          				<td>Number of days per week</td>
				          				<td>-</td>
				          				<td>5 days/week</td>
				          			</tr>
				          			<tr>
				          				<td>Committed hours per week </td>
				          				<td>-</td>
				          				<td>5 Hrs.</td>
				          			</tr>
				          		</tbody>
				          	</table>

				          	<h6>FAQs of the Program</h6>
				          	<blockquote>
				          		<h5>Question 1: What is the tentative timetable of the program?</h5> 
								<p><b>Answer:</b> Classes are held on 4 days a week – Tuesday, Thursday, Friday and Saturday, <b>A total of 5 classes are held per week – 1-phy, 1-chem, 1- Bio and 2- Maths.</b> Classes are held in the evening time between 4:00 pm to 6:15 pm. Exact timing can be enquired at the center. During summer holidays and school breaks, students are called in addition to the above timetable to cover the course. </p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Question 2: What is the test schedule and how is feedback given to parents.</h5>
				          		<p><b>Answer:</b> Tests are held on fortnightly basis on alternative Tuesdays (exact timetable will be given at the commencement of the program). The test results are displayed on the center’s notice board within 2-3 working days with marks and ranks of the students. Parents can also enquire the performance on phone.</p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Question 4: For which places is cab facility provided.</h5>
				          		<p><b>Answer:</b> Cab facility is provided from MMX Mall, Air Force Station Hindon Gate, Shalimar Garden (Shiv Chowk and 80 ft Rd), Rajender Nagar Sec-3, Sec-2 and Sec-5, Shyam Park Extn. Jindal Market, Mohan Nagar (Parsvnath), Lajpat Nagar. Additional locations are added if there are at least 3 students from the locality. The monthly charges for a location can be inquired at the center. Payment of cab fees has to be done on quarterly basis in advance.</p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Questions 5:  What are the fees installments?</h5>
				          		<p><b>Answers:</b> The fees is payable in 2 installments: <br> <b>Ist Installment</b> At Admission (before April 2014) – 50%, <b>Second Installment</b> – 5 months from admission (August 2014) - remaining,<br> Additional discount is provided if full payment of the program is done in first installment. </p>
				          	</blockquote>
				          	<blockquote>
				          		<h5>Questions 9:  What study material is given in the program?</h5>
				          		<p><b>Answer:</b> Complete study package is given in form of booklets and assignments for each topic. These booklets and assignment cover theory, <br> <b>However, few books are a must for each student to buy for the success of the program as they are referred in the classes:</b>  <br>NCERT books – Maths and science <br>R.D Sharma (math) <br>Ulinke Sample Papers</p>
				          	</blockquote>
        				</div>
        			</div>
        		</div>

        		<div class="large-4 medium-12 columns left-main">
        			<div class="">
		                <div class="heading">
		                  <h2><span>GreyCells on facbook</span></h2>
		                </div>

		                <div class="fb-feeds">
		                  <img src="img/fb.png">
		                </div>

		                <div class="video-container">
		                  <div class="heading">
		                    <h2><span>virtual tour of GreyCells Center</span></h2>
		                  </div>

		                  <div class="video">
		                    <iframe width="100%" height="250" src="//www.youtube.com/embed/Hh6B362ffbk" frameborder="0" allowfullscreen=""></iframe>
		                  </div>
		                </div>
	                </div>
        		</div>
        	</div>
        </div>
        
   <?php
              include 'footer.php';
              ?>
      </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
