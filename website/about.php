<!doctype html>
<php lang="en">
  <?php 
  include'head.php';
  ?>
  <body>
    
    <div class="main-container">
      <div class="container">
        <div class="header">
          <div class="row">
            <div class="large-12 columns">
              <div class="logo">
                <a href="index.php"><img src="img/logo.jpg" style="width:262px;"></a>
              </div>
            </div>
            	<?php
            	include 'nav.php';
            	?>
          </div>
        </div>

        <div class="content-container">
        	<div class="row">
        		<div class="large-8 medium-8 columns">
        			<div class="content">
        				<div class="heading">
        					<h2><span>About GreyCells</span></h2>
        				</div>

        				<div class="text">
        					<p>GreyCells is an institute started by IITians in 2009 with a vision of providing best quality coaching for IITJEE / AIEEE /CBSE in Sahibabad. We are a team of IITians having a common passion- To Impart High Quality Education. We realized that Sahibabad did not have any reliable coaching institute for IITJEE / AIEEE / UPTECH / IP.</p>
        					<p>With the changed pattern of IITJEE, we at GreyCells believe that the future belongs to those who concentrate on IITJEE preparation along with the CBSE syllabus.</p>
        					<p>Presently we have evening batches running at our Rajender Nagar center, which comprises of students from adjoining schools like K.V-1, K.V-2, DAAV, DLF, Airforce hindon, Airforce Balbharti, Vivekanand(delhi), DPS etc.</p>
        					<p>Also we have a school project running successfully for the last 3 years with DLF public school in which a separate section is created (dedicated for students targeting both IITJEE and CBSE simultaneously) and all the Physics, Chemistry and Maths classes are taken up by GreyCells Mentors.</p>
        					<p>Previously students aspiring for engineering exams had to go to Delhi / Kota to get coaching. This results in wastage of precious time, energy and money of the students and parents. With GreyCells around the corner, now you do not have to go to a far off place to get the expert guidance. The best coaching for IITJEE / AIEEE is now available @ Sahibabad.</p>
        					<p>At GreyCells we are committed to provide you the best of the faculty, study material and time to time motivation and counseling so that you simply become unstoppable to get into IITJEE / AIEEE and other competitive exams. We believe that there is an unnecessary stigma and hype around academics especially for preparation of prestigious IIT JEE. To qualify for IITJEE, you do not need to be super intelligent but rather sincere. At GreyCells, you have the advantage of getting trained by IITians – who can guide you at every step of your preparation because they themselves have been there and done that.</p>

        					<h6>virtual tour of GreyCells Center</h6>
        					<div class="video">
                    <iframe width="100%" height="315" src="//www.youtube.com/embed/1lBUhBmi5HQ?list=UUHTavI0aE6MfKabqh0JxcZw" frameborder="0" allowfullscreen></iframe>
                  </div>

                <h6>Advantage GreyCells</h6>
		    				<ol>
		    					<li><p><b>All Faculty members are IITian themselves and will teach throughout the program:</b> In coaching institutes like FIITJEE, Vidyamandir – the permanency of teachers are not guaranteed as they are working as employee and if they are offered better compensation somewhere else they leave their current institutes. At GreyCells all teachers are stakeholders hence they will teach throughout the program. No other institute reveals the qualification and makes you interact with teachers directly, which is not the case at GreyCells.</p></li>
		    					<li><p><b>Direct Interaction with Teachers on regular basis: </b>Each time you visit the center, the students and parents can directly interact with the teachers of the program, communicating their doubts and queries. This interaction does not happen in any other institute. At GreyCells we believe that regular interaction between students, teachers and parents is necessary for success of the program.</p></li>
		    					<li><p><b>CBSE/School course covered along with Competitive course: </b>The program covers CBSE along with Competitive syllabus. Though the syllabus is almost same, but the natures of questions are different in CBSE and Competition. For CBSE a student needs to concentrate for subjective questions, while in competition the questions are objective. At GreyCells we concentrate at both the aspects. At GreyCells, we follow the KOTA methodology in which we cover every type of question on every topic inside the classroom and do not leave any concept for self study. This makes the program exhaustive and hence the student is comfortable both at school and in competition. <br><br>There are many students studying at GreyCells who had earlier joined institutes like FIITJEE, Vidyamandir, Narayana etc. and found it tough to cope up there as there the preparation was selective in nature and the complete syllabus was not covered. After joining GreyCells these students find it easy to understand each topic in detail and are able to apply concepts in a better way. They are comfortable at their schools and are on way to crack engineering competitive examinations.</p></li>
	    					</ol>
        				</div>
        			</div>
        		</div>

        		<div class="large-4 medium-12 columns left-main">
        			<div class="">
		                <div class="heading">
		                  <h2><span>GreyCells on facbook</span></h2>
		                </div>

		                <div class="fb-feeds">
		                  <img src="img/fb.png">
		                </div>

		                <div class="video-container">
		                  <div class="heading">
		                    <h2><span>virtual tour of GreyCells Center</span></h2>
		                  </div>

		                  <div class="video">
		                    <iframe width="100%" height="315" src="//www.youtube.com/embed/1lBUhBmi5HQ?list=UUHTavI0aE6MfKabqh0JxcZw" frameborder="0" allowfullscreen></iframe>
		                  </div>
		                </div>
	                </div>
        		</div>
        	</div>
        </div>
        
       <?php
       	include'footer.php';
       ?>
      </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</php>
