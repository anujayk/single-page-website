<!doctype html>
<html class="no-js" lang="en">
   <?php
  include'head.php'
  ?>
  <body>
    
    <div class="main-container">
      <div class="container">
        <div class="header">
          <div class="row">
            <div class="large-12 columns">
              <div class="logo">
                <a href="index.php"><img src="img/logo.jpg" style="width:262px;"></a>
              </div>
            </div>

           <?php
              include 'nav.php';
              ?>
          </div>
        </div>

        <section>
        	<div class="row">
        		<div class="large-12 columns">
        			<h1 class="coming">Coming Soon....</h1>
        		</div>
        	</div>
        </section>	
        
       <?php
        include'footer.php';
       ?>
      </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
